<div id="footer">
<ul class="no_print">
<li><a href="http://www.sonsbeekmedia.nl">Home page</a> | </li>
<li><a href="https://github.com/bmbrands">Projects on Github</a> | </li>
<li><a href="http://www.somerandomthoughts.com/blog/">Some Random Thoughts</a> | </li> 
<li><a href="http://www.moodlenews.com">Moodle News</a> | </li> 
<li><a href="http://www.linkedin.com/pub/dir/bas/brands">LinkedIn</a> | </li> 
</ul>
<p>Sonsbeekmedia, Arnhem, The Netherlands </p>
<p>&copy; Sonsbeekmedia 2012</p>
</div>