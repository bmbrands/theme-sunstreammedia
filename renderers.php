<?php
class theme_sonsbeekmedia_core_renderer extends core_renderer {

    public function heading($text, $level = 2, $classes = 'main', $id = null) {
    global $COURSE;
    	
    $topicoutline = get_string('topicoutline');
    
    if ($text == $topicoutline) {
    	$text = $COURSE->fullname;
    }
    
    $content = parent::heading($text, $level, $classes, $id);
 
    return $content;
}
        
    
    //end class
 
}